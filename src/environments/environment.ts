// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAsCh0GYkvjY7S9rZROI7atp9f7LrKnxV0",
    authDomain: "tweets-fdc20.firebaseapp.com",
    databaseURL: "https://tweets-fdc20.firebaseio.com",
    projectId: "tweets-fdc20",
    storageBucket: "tweets-fdc20.appspot.com",
    messagingSenderId: "346151145509",
    appId: "1:346151145509:web:ae32cc497e653767"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
