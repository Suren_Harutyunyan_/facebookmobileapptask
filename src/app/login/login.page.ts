import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../api/login.service';
import {Facebook} from '@ionic-native/facebook/ngx';



@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    constructor(
        private router: Router,
        private loginService: LoginService) {
    }

    ngOnInit() {

    }

    loginFb() {
        this.loginService.login().then((result) => {
            this.router.navigate(['/home']);
        }).catch((err) => {
            window.location.reload();
        });
    }


}
