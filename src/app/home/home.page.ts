import {Component, OnInit} from '@angular/core';
import {SearchService} from '../api/search.service';
import {Router} from '@angular/router';
import {LoginService} from '../api/login.service';
import {User} from '../user';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    searchData: any;
    profileImg: string;
    places: any;
    searchValue: string;
    userData: User;
    constructor(
                private searchService: SearchService,
                private router: Router,
                private loginService: LoginService) {

    }

    ngOnInit() {
        if (localStorage.getItem('user_data')) {
            this.userData = JSON.parse(localStorage.getItem('user_data'));
        }
    }
    // searching places in facebook
    // param(search) - input value
    search(search) {
        this.searchService.search(search).subscribe((res: any) => {
            this.searchData = res;
        });
    }
    // get user likes pages
    getLikesPages() {
        this.searchService.getLikesPages().subscribe((res: any) => {
            this.searchData = res;
        });
    }

    // get user profile picture another way
    getProfilePicture() {
        this.searchService.getProfilePicture(this.userData.id).subscribe((res: any) => {
            this.profileImg = res.picture.data.url;
        });
    }
    // get place by coorditnantes
    // with latitude longitude
    getPlacesByCoordinantes() {
        this.searchService.getPlaces().subscribe((res: any) => {
            this.places = res;
        });
    }

    // open new window in browser and show current facebook page
    // params (id) - page id
    goToPlace(id) {
        this.searchService.getPlace(id).subscribe((res: any) => {
            if (res && res.link) {
                window.open(res.link, '_system', 'location=yes');
            }
        });
    }

    logOut() {
        this.loginService.logOut().then(() => {
            this.router.navigate(['/login']);
        }).catch(() => {
            window.location.reload();
        });
    }

}
