import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Facebook} from '@ionic-native/facebook/ngx';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private fb: Facebook) {

    }

    canActivate(route: ActivatedRouteSnapshot): boolean {


        let authInfo = {
            authenticated: false
        };
        this.fb.getLoginStatus().then((result) => {
            authInfo.authenticated = result.status == 'connected';
        });

        if (!authInfo.authenticated) {
            this.router.navigate(['login']);
            return false;
        }

        return true;

    }
}
