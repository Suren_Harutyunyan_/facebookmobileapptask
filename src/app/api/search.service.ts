import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Facebook} from '@ionic-native/facebook/ngx';
import {Router} from '@angular/router';
import {LoginService} from './login.service';

@Injectable({
    providedIn: 'root'
})
export class SearchService {
    private ROOT_URL = 'https://graph.facebook.com';
    private token: string;

    constructor(private http: HttpClient, private fb: Facebook, private router: Router, private loginService: LoginService) {
        this.fb.getAccessToken().then(result => {
            this.token = result;
        }).catch((err) => {
            this.loginService.logOut().then(() => {
                this.router.navigate(['/login']);
            }).catch(() => {
                window.location.reload();
            });
        });
    }
    // Search place on Facebook
    // param query - place name
    search(query: string) {
        return this.http.get(`${this.ROOT_URL}` + '/search?q=' + query + '&type=place&' + 'access_token=' + this.token);
    }

    // get user likes pages
    getLikesPages() {
        return this.http.get(`${this.ROOT_URL}` + '/me?fields=id,name,likes&' + 'access_token=' + this.token);
    }
    // get user more likes pages
    // for pagination
    getMoreLikesPages(next) {
        return next;
    }
    // get user information another way
    getUser(id) {
        return this.http.get(`${this.ROOT_URL}/` + id + '?access_token=' + this.token);
    }
    // get user profile picture another way
    getProfilePicture(id) {
        return this.http.get(`${this.ROOT_URL}/` + id + '?fields=picture&' + 'access_token=' + this.token);
    }
    // get places by coordinates
    // with latitude longitude
    getPlaces() {
        return this.http.get(`${this.ROOT_URL}` + '/search?type=place&center=40.7304,-73.9921&distance=1000&categories=["FOOD_BEVERAGE"]&fields=name,about,single_line_address' + '&access_token=' + this.token);
    }
    // get current place link for navigation to facebook page
    getPlace(id) {
        return this.http.get(`${this.ROOT_URL}/` + id + '?fields=link&access_token=' + this.token);
    }
}
