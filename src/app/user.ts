export interface User {
    id: string;
    picture: string;
    username: string;
    first_name: string;
    email: string;
}
