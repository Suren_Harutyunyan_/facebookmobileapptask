import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook/ngx';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private http: HttpClient, private fb: Facebook, private router: Router) {
    }

    login() {
        return new Promise((resolve, reject) => {
            this.fb.login(['public_profile', 'user_friends', 'email'])
                .then((res: FacebookLoginResponse) => {
                    if (res.status == 'connected') {
                        try {
                            this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
                                const userData = {
                                    email: profile['email'],
                                    first_name: profile['first_name'],
                                    picture: profile['picture_large']['data']['url'],
                                    username: profile['name'],
                                    id: profile['id']
                                };
                                localStorage.setItem('user_data', JSON.stringify(userData));
                                resolve(true);
                            });
                        } catch (e) {
                            alert(e);
                        }
                    } else {
                        alert('An error occurred while loging in');
                    }


                })
                .catch((e) => {
                    reject(false);
                });
            this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);

        });

    }

    checkToken(id, token) {
        return this.http.get('https://graph.facebook.com/' + id + '?access_token=' + token);
    }

    logOut() {
        return new Promise((resolve, reject) => {
            this.fb.logout().then((result) => {
                localStorage.removeItem('user_data');
                resolve(true);
            }).catch(() => {
                reject(false);
            });
        });
    }

}
